# clj-spec-tools

Tools/functions for working with Clojure spec.

Features:

- Taking unqualified maps and returning a qualified map (validated)
- Spec validation using expound for output
- Ability to add custom validation functions
- Ability to add defaults


## Disclaimer

**THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.**
